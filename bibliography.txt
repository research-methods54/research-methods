References:
[1]L. Rabiner, "A tutorial on hidden Markov models and selected applications in speech recognition", Proceedings of the IEEE, vol. 77, no. 2, pp. 257-286, 1989. Available: 10.1109/5.18626 [Accessed 26 July 2021].

[2]K. Khalil and C. Adnan, "Implementation of speech synthesis based on HMM using PADAS database", 2015 IEEE 12th International Multi-Conference on Systems, Signals & Devices (SSD15), 2015. Available: 10.1109/ssd.2015.7348133 [Accessed 26 July 2021].

[3]G. Ramya and N. Naik, "Implementation of telugu speech synthesis system", 2017 International Conference on Advances in Computing, Communications and Informatics (ICACCI), 2017. Available: 10.1109/icacci.2017.8125997 [Accessed 26 July 2021].

[4]M. Anil and S. Shirbahadurkar, "Speech modification for prosody conversion in expressive Marathi text-to-speech synthesis", 2014 International Conference on Signal Processing and Integrated Networks (SPIN), 2014. Available: 10.1109/spin.2014.6776921 [Accessed 26 July 2021].

[5]R. Saikia, S. Singh and P. Sarmah, "Effect of language independent transcribers on spoken language identification for different Indian languages", 2017 International Conference on Asian Language Processing (IALP), 2017. Available: 10.1109/ialp.2017.8300582 [Accessed 26 July 2021].

[6]S. Kannojia, G. Singh and S. Mathur, "A text to speech synthesizer using acoustic unit based concatenation for any Indian language of Devanagari script", 2016 11th International Conference on Industrial and Information Systems (ICIIS), 2016. Available: 10.1109/iciinfs.2016.8263040 [Accessed 26 July 2021].

[7]M. Jeeva, B. Ramani and P. Vijayalakshmi, "Performance evaluation and comparison of multilingual speech synthesizers for Indian languages", 2013 International Conference on Recent Trends in Information Technology (ICRTIT), 2013. Available: 10.1109/icrtit.2013.6844268 [Accessed 26 July 2021].

[8]S. Mandal, H. Choudhury, S. Prasanna and S. Sundaram, "DNN-HMM Based Large Vocabulary Online Handwritten Assamese Word Recognition System", 2018 16th International Conference on Frontiers in Handwriting Recognition (ICFHR), 2018. Available: 10.1109/icfhr-2018.2018.00063 [Accessed 26 July 2021].

[9]K. Oura, K. Tokuda, J. Yamagishi, S. King and M. Wester, "Unsupervised cross-lingual speaker adaptation for HMM-based speech synthesis", 2010 IEEE International Conference on Acoustics, Speech and Signal Processing, 2010. Available: 10.1109/icassp.2010.5495558 [Accessed 26 July 2021].

[10]H. Lu, Z. Ling, L. Dai and R. Wang, "Building HMM based unit-selection speech synthesis system using synthetic speech naturalness evaluation score", 2011 IEEE International Conference on Acoustics, Speech and Signal Processing (ICASSP), 2011. Available: 10.1109/icassp.2011.5947567 [Accessed 26 July 2021].