Research Question:
How HMM-based Speech Synthesis Mechanism Can be Used for The Indic Language Generation.  

Search String:
text to speech indian languages hmm (15 matches)

Dismissed Search Strings:
- Hidden markov models (30,675 matches)
- hidden markov models speech synthesis (1,377 matches)
- speech synthesis text to speech (2230 matches)
- hmm speech synthesis (659 matches)
- text to speech indian languages (194 matches)

Inclusion Criteria:
- The study includes detailed analysis of hidden markov models.
- Study includes details of other possible speech synthesis methods.
- The study is well referenced .
- The study includes any novel ideas or methods.

Exclusion criteria:
- The study isnt applicable to indian languages.
- The study doesnt present enough evidence in support of it claims
- The study doesnt express the probable applications of its methods

Sources/search engines:
 - IEEE Xplore

